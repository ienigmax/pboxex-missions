## P-Box Missions

#### Description 
Goal of this project is to demonstrate an API built on Express and TypeScript.

This application serves as the microservice for preparing CRUD operations upon missions model. 
Due to the nature of the project - all the communication between the API and this service is end to end encrypted .

This project
#### Requirements
- `node.js` > 8 
- `MongoDB` must be installed and configured. It is recommended to install Robo 3t or any other IDE 
           to work with the database

#### Run Project
The project supports multiple environments. 

`npm run dev` - `development` environment (make sure to copy the properties from `.env.example` file)

`npm run prod` - run `production` environment

`pm2 start npm --name=papp-missions -- run prod` - Using pm2 to run the service as a background process

#### References
* [Main API (Auth Service)](https://bitbucket.org/ienigmax/pboxex-server/src/master/)
* [Client UI (Angular)](https://bitbucket.org/ienigmax/pboxex-client/src/master/)
