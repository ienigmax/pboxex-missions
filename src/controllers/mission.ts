import {Request, Response, Router} from "express";
import {missionModel} from "../models";
import {AES, enc} from 'crypto-ts';

export const missionController = Router();

missionController.get('/list', getEntityList);
missionController.post('/', createEntity);
missionController.get('/:key', getEntity);
missionController.put('/:key', updateEntity);
missionController.delete('/:key', deleteEntity);

let base64Decode = (str) => Buffer.from(str, 'base64').toString('utf8');

async function getEntityList(req: Request, res: Response) {
    try {
        const { limit, page } = req.query;
        // @ts-ignore
        let list = await missionModel.find().skip((( +page - 1) * +limit)).limit(+limit).sort([['createdAt', -1]])
        let count = await missionModel.count();
        const crypt = AES.encrypt(JSON.stringify({list, count}), process.env.CRYPT_SALT).toString();
        // console.log(process.env.CRYPT_SALT, crypt)
        return res.status(200).json({msg: crypt});
    } catch (e) {
        console.error(e);
        return res.status(400).json({msg: 'Could not get missions list'});
    }
}

async function getEntity(req: Request, res: Response) {
    try {
        let { key } = req.params;
        let entity = await missionModel.findOne({ key });
        if(!entity) return res.status(400).json({msg: 'could not get mission'});
        const crypt = AES.encrypt(JSON.stringify(entity), process.env.CRYPT_SALT).toString();
        return res.status(200).json({msg: crypt});
        // return res.status(200).json(entity);
    } catch (e) {
        console.error(e);
        return res.status(400).json({msg: 'Could not get mission'});
    }
}

async function createEntity(req: Request, res: Response) {
    try {
        // Decrypt the message from master API
        const bytes  = AES.decrypt(req.body.msg.toString(), process.env.CRYPT_SALT);
        const decryptedData = JSON.parse(bytes.toString(enc.Utf8));

        const { key, value } = decryptedData;

        console.log({key, value})

        // Check if key and value are missing
        if(!key) {
            let msg = AES.encrypt('key missing', process.env.CRYPT_SALT).toString()
            return res.status(400).json({ msg });
        }
        if(!value) {
            let msg = AES.encrypt('value missing', process.env.CRYPT_SALT).toString()
            return res.status(400).json({ msg });
        }

        // Check if the mission already exists
        let entity = await missionModel.findOne({ key });
        if(entity) {
            let msg = AES.encrypt('Mission already exists', process.env.CRYPT_SALT).toString()
            return res.status(400).json({ msg });
        }

        // If not create
        let result = await missionModel.create({ key, value });
        const crypt = AES.encrypt(JSON.stringify(result), process.env.CRYPT_SALT).toString();
        return res.status(200).json({ msg: crypt });
        // return res.status(200).json(result);
    } catch (e) {
        console.error(e);
        return res.status(400).json({ msg: 'Could not create mission' });
    }
}

async function updateEntity(req: Request, res: Response) {
    try {
        // Decrypt the message from master API
        const bytes  = AES.decrypt(req.body.msg.toString(), process.env.CRYPT_SALT);
        console.log('--', bytes.toString(enc.Utf8))
        const decryptedData = JSON.parse(bytes.toString(enc.Utf8));

        const key = req.params.key;
        const { key: newKey, value } = decryptedData;

        // Check if the mission already exists
        let entity = await missionModel.find({ key });
        if(!entity) {
            let msg = AES.encrypt('Mission not exists', process.env.CRYPT_SALT).toString()
            return res.status(400).json({ msg });
        }

        // Update and inform
        let result = await missionModel.updateOne({ key }, { key: newKey, value });
        const crypt = AES.encrypt(JSON.stringify(result), process.env.CRYPT_SALT).toString();
        return res.status(200).json({ msg: crypt });
    } catch (e) {
        console.error(e);
        return res.status(400).json({msg: 'Could not edit mission'});
    }
}

async function deleteEntity(req: Request, res: Response) {
    try {
        // Decrypt the message from master API
        const bytes  = AES.decrypt(base64Decode(req.params.key), process.env.CRYPT_SALT);

        // Assign key the decrypted value
        const key = bytes.toString(enc.Utf8)

        // Delete and return
        let result = await missionModel.deleteOne({ key });
        const crypt = AES.encrypt(JSON.stringify(result), process.env.CRYPT_SALT).toString();
        return res.status(200).json({ msg: crypt });
    } catch (e) {
        console.error(e);
        return res.status(400).json({msg: 'Could not create mission'});
    }
}

export default { missionController };
