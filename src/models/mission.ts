import { Document, Schema, Model, model, Error } from "mongoose";

interface IMission extends Document {
   key: string;
   value: object;
}

const missionSchema = new Schema({
    key: { type: String },
    value: { type: Object }
},{
    toJSON: {
        virtuals: true,
        getters: true,
    },
    timestamps: true
});

export const missionModel: Model<IMission> = model<IMission>('Mission', missionSchema);
