import { Router } from 'express';
import {missionController} from '../controllers'
const router = Router();
router.use('/api', missionController);
export default router;
