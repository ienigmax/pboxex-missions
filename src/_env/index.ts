const { NODE_ENV } = process.env;
const path = require('path').resolve(__dirname, './', '.env.' + NODE_ENV);
require('dotenv').config({ path });

