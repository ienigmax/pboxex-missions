// @ts-ignore
import './_env';
// @ts-ignore
import express from 'express';
// @ts-ignore
import router from './router/main'
import bodyParser from 'body-parser'
import db from './db';
db().then();
const { PORT, HOST, NODE_ENV} = process.env;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);

// @ts-ignore
app.listen(PORT, HOST, () => {
    console.log(`${NODE_ENV} server is listening on ${process.env.PROTOCOL}://${HOST}:${PORT}`);
});
