import * as crypto from 'crypto';

interface IConfig {
    algorithm?: string;
    encryptionKey?: string;
    salt?: string;
    iv?: Buffer;
}

class Encryption {
    private readonly algorithm: string;
    private readonly key: Buffer | string;
    private readonly salt: string;
    private readonly iv: Buffer | null;

    constructor(config: IConfig) {
        this.algorithm = config.algorithm || "";
        this.salt = config.salt || "";
        const ENCRYPTION_KEY = config.encryptionKey ? Buffer.from(config.encryptionKey).toString('hex') : '';
        this.key = ENCRYPTION_KEY ? Buffer.from(ENCRYPTION_KEY, "hex") : '';
        this.iv = config.iv || null;
        if (!this.algorithm && !this.key) {
            throw Error('Configuration Error!');
        }
    }

    public encrypt(data: string | Array<any> | object): string {
        // If missing, throw exception
        if(!data) throw new Error('payload missing');

        // Check the type of the object
        if(typeof data === "object") data = JSON.stringify(data);
        else if(typeof data === 'number') data = `${data}`;

        // set the cipher
        const cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv);

        // encode to the buffer
        let buffer = Buffer.from(data, 'utf8').toString("binary") + cipher.final("base64");
        return cipher.update(buffer, "binary", "base64")
    }

    public decrypt(secret: string) {
        // If missing, throw exception
        if(!secret) throw new Error('secret missing');

        // Initialize Decipher instance
        const decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv);

        // encodes encrypted value from base64 to hex
        const buffer = Buffer.from(secret, "base64").toString("hex");
        const decrypted = decipher.update(buffer, 'hex', 'base64') + decipher.final('base64');
        const buf = Buffer.from(decrypted, "base64");
        return buf.toString('utf8');
    }
}
const sha1 = (str) => crypto.createHash('sha1').update(str).digest('hex');
export const encryptionLibrary = new Encryption({
    algorithm: process.env.CRYPT_ALGO,
    encryptionKey: sha1(process.env.CRYPTO_KEY),
    salt: process.env.CRYPT_SALT,
    iv: Buffer.from(sha1(process.env.CRYPTO_IV), 'utf8'),
});
