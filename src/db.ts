import mongoose from 'mongoose'
const {
    DB_HOST,
    DB_USER,
    DB_PASS,
    DB_PORT,
    DB_DEFALT,
} = process.env;

export default async () => {
    try {
        console.log(`mongodb://${DB_HOST}:${DB_PORT}/${DB_DEFALT}`); // For testing purposes
        let connectionString = DB_USER && DB_PASS ?
            `mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_DEFALT}` :
            `mongodb://${DB_HOST}:${DB_PORT}/${DB_DEFALT}`
        let connection = await mongoose.connect(connectionString, {
            useNewUrlParser: true,
            keepAlive: true,
            keepAliveInitialDelay: 300000,
            useUnifiedTopology: true
        });
        if(!connection) process.exit(1);
        else console.info(`Connection to ${DB_HOST}:${DB_PORT} established`);
    } catch (e) {
        console.error(e);
    }
}
